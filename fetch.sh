#!/usr/bin/env bash
# ╺┳╸┏━┓┏━╸┏━┓╻┏━┓   ┏━╸┏━╸╺┳╸┏━╸╻ ╻
#  ┃ ┣━┫┃  ┃ ┃ ┗━┓   ┣╸ ┣╸  ┃ ┃  ┣━┫
#  ╹ ╹ ╹┗━╸┗━┛ ┗━┛   ╹  ┗━╸ ╹ ┗━╸╹ ╹
#       its crap but yer usin it :D
# 
# fetch - https://gitlab.com/YOUR-WORST-TACO/fetch.git
#
# Copyright (C) 2018 Stephen Tafoya <stephentafoya@outlook.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

#################################################################
# TO DO LIST
# TO DO LIST
# TO DO LIST
#################################################################
#################################################################


#################################################################
# ┏━╸┏━┓┏━╸┏━┓╺┳╸┏━╸   ┏━╸┏━┓┏┓╻┏━╸╻┏━╸
# ┃  ┣┳┛┣╸ ┣━┫ ┃ ┣╸    ┃  ┃ ┃┃┗┫┣╸ ┃┃╺┓
# ┗━╸╹┗╸┗━╸╹ ╹ ╹ ┗━╸   ┗━╸┗━┛╹ ╹╹  ╹┗━┛
#################################################################
#################################################################

config_dir="${XDG_CONFIG_HOME:-$HOME/.config}/taco-fetch"

if [ ! -f "$config_dir"/fetch.cfg ]; then
    mkdir -p "$config_dir"
    cat <<EOF > "$config_dir"/fetch.cfg
# Example config
# you must have an output function similar to the one below
# note the example out vm getvolume demonstrates adding your own function
output()
{
    #out vm getvolume
    out os getos
    out mu getram used
    out mt getram total
    out sh getshell
    out wm getwm
    out te getterm
    out pk getpackages
}

# to add a function add it to the array extra_functions
extra_functions=("getvolume")

# then create the function
# Note: it must echo -n or printf the text you want it to display
getvolume()
{
    echo -n "$(mpc volume)"
}
EOF
fi

source "$config_dir"/fetch.cfg

#################################################################
# ┏━╸┏━┓╺┳╸┏━╸╻ ╻   ┏━╸╻ ╻╻╺┳╸
# ┃  ┣━┫ ┃ ┃  ┣━┫   ┣╸ ┏╋┛┃ ┃ 
# ┗━╸╹ ╹ ╹ ┗━╸╹ ╹   ┗━╸╹ ╹╹ ╹ 
#################################################################
#################################################################
cleanup()
{
    tput cnorm
    echo -e "\033[0m"
    if [ ! "$DEBUG" = "true" ]; then
        clear
    fi
}

trap cleanup EXIT
trap cleanup SIGINT

#################################################################
#################################################################
#################################################################

W=$(tput cols)
H=$(tput lines)

DEBUG="false"
speed="0.04"
default_width="12"
default_funcs=("getos" "getram" "getshell" "getwm" "getterm" "getpackages")

dblog()
{
    if [ "$DEBUG" = "true" ]; then
        echo "DEBUG LOG: $@"
    fi
}

usage()
{
    printf "%s\n\t%s\n\t%s\n\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n\t%s\n" \
        "fetch - Find Every Tiny Crap tHing" \
        "a fetch script I made that has a fun little animation" \
        "Usage: fetch [OPTIONS]... " \
        "-h     --help                  display this message" \
        "-d     --debug                 debug" \
        "-x                             command readout" \
        "-G                             display ram in GB" \
        "-s     [speed](default 0.04)   change animation speed" \
        "-a     [ascii file]            give an ascii file"
}

while [ ! "$#" -eq 0 ]; do
    case "$1" in
        "-h"|"--help" )
            usage; DEBUG="true"
            exit
            ;;
        "-d"|"--debug" )
            DEBUG="true"
            ;;
        "-x" )
            set -x
            ;;
        "-G" )
            GB="true"
            ;;
        "-s" )
            if [ "${2:0:1}" = "-" ] || [ -z "$2" ]; then
                echo "Error, sleep amount expected, try fetch -h"
                exit 1
            fi
            speed="$2"
            shift
            ;;
        * )
            echo "Unrecognized Option, try -h for help"
            exit 1
            ;;
    esac
    shift
done


##################################################################
# ┏━┓┏━┓   ╺┳┓┏━╸╺┳╸┏━╸┏━╸╺┳╸╻┏━┓┏┓╻
# ┃ ┃┗━┓    ┃┃┣╸  ┃ ┣╸ ┃   ┃ ┃┃ ┃┃┗┫
# ┗━┛┗━┛   ╺┻┛┗━╸ ╹ ┗━╸┗━╸ ╹ ╹┗━┛╹ ╹
##################################################################
##################################################################
# try to use os-release to detect os
# supported operating systems for this method:
#       - arch
#       - void
##################################################################
getos()
{
    opsys="unknown"
    if [ -f "/etc/os-release" ]; then # detect using os-release
        while read line; do
            varname="$(echo "$line" | cut -d '=' -f 1)"
            if [ "$varname" = "ID" ]; then
                osid="$(echo "${line//\"}" | cut -d '=' -f 2-)"
                case "$osid" in
                    "arch" )
                        opsys="arch"
                        ;;
                    "void" )
                        opsys="void"
                        ;;
                    * )
                        opsys="$osid"
                        ;;
                esac
            fi
        done < /etc/os-release
        release="$(cat /etc/os-release)"
    fi

    #################################################################
    # if previous method failed try to detect with uname
    # supported operating systems for this method:
    #       - openbsd
    #################################################################
    if command -v "uname" $> /dev/null && \
        [ "$opsys" = "unknown" ]; then
        varname="$(uname)"
        case "$varname" in
            "OpenBSD" )
                opsys="openbsd"
                ;;
        esac
    fi

    echo -n "$opsys"
}


#################################################################
# ┏━┓┏━┓┏┳┓   ╺┳┓┏━╸╺┳╸┏━╸┏━╸╺┳╸╻┏━┓┏┓╻
# ┣┳┛┣━┫┃┃┃    ┃┃┣╸  ┃ ┣╸ ┃   ┃ ┃┃ ┃┃┗┫
# ╹┗╸╹ ╹╹ ╹   ╺┻┛┗━╸ ╹ ┗━╸┗━╸ ╹ ╹┗━┛╹ ╹
#################################################################
#################################################################
# detect ram with meminfo @ /proc/meminfo
#################################################################
getram()
{
    if [ -z "$totram" ]; then
        calcram
    fi
    case $1 in
        "used" )
            echo -n "$usedram $unitram"
        ;;
        "total" )
            echo -n "$totram $unitram"
        ;;
        * )
            echo -n "$usedram / $totram $unitram"
        ;;
    esac
}

calcram()
{
    totram="0"
    freeram="0"
    usedram="0"
    if [ -f "/proc/meminfo" ]; then
        while IFS=":" read -r a b; do
            case "$a" in
                "MemTotal") ((usedram+=${b/kB})); totram="${b/kB}" ;;
                "Shmem") ((usedram+=${b/kB})) ;;
                "MemFree" | "Buffers" | "Cached" | "SReclaimable" )
                    usedram="$((usedram-=${b/kB}))"
                ;;
            esac
        done < /proc/meminfo
        
        usedram="$((usedram / 1024))"
        totram="$((totram / 1024))"
        unitram="MB"

        if [ "$GB" = "true" ]; then
            usedram="$((((($usedram + 5) / 10) + 5) / 10))"
            totram="$((((($totram + 5) / 10) + 5) / 10))"

            usedram_offset="$((${#usedram} - 1))"
            if [ "${usedram: -1}" = "0" ]; then
                usedram="${usedram:0:$usedram_offset}"
            else
                usedram="${usedram:0:$usedram_offset}.${usedram:$usedram_offset}"
            fi
            totram_offset="$((${#totram} - 1))"
            if [ "${totram: -1}" = "0" ]; then
                totram="${totram:0:$totram_offset}"
            else
                totram="${totram:0:$totram_offset}.${totram:$totram_offset}"
            fi
            unitram="GB"
        fi
    fi
}

#################################################################
# ┏━╸┏━╸╺┳╸   ┏━┓╻ ╻┏━╸╻  ╻  
# ┃╺┓┣╸  ┃    ┗━┓┣━┫┣╸ ┃  ┃  
# ┗━┛┗━╸ ╹    ┗━┛╹ ╹┗━╸┗━╸┗━╸
#################################################################
#################################################################
# get the current shell
#################################################################
getshell()
{
    shell="$(basename $SHELL)"
    case "$shell" in
        "zsh" )
            shell="$shell $($SHELL --version | cut -d ' ' -f 2)"
            ;;
        "*" )
            ;;
    esac
    echo -n "$shell"
}

#################################################################
# ╺┳┓┏━╸╺┳╸┏━╸┏━╸╺┳╸   ╻ ╻┏┳┓
#  ┃┃┣╸  ┃ ┣╸ ┃   ┃    ┃╻┃┃┃┃
# ╺┻┛┗━╸ ╹ ┗━╸┗━╸ ╹    ┗┻┛╹ ╹
#################################################################
#################################################################
# Detect current wm with XDG_CURRENT_DESKTOP
# if that fails try using GDMSESSION
# and if all else fails try using wmctrl -m
#################################################################
getwm()
{
    window_manager="Maybe TTY?"
    if [ ! -z "$XDG_CURRENT_DESKTOP" ]; then
        window_manager="$XDG_CURRENT_DESKTOP"
    elif [ ! -z "$GDMSESSION" ]; then
        window_manager="$GDMSESSION"
    elif command -v wmctrl $> /dev/null &&\
        wmctrl -m 2> /dev/null &> /dev/null; then
        window_manager="$(wmctrl -m | grep 'Name:' | cut -d' ' -f2)"
    else
        for wm in 2bwm bspwm openbox; do
            if ps -a | grep -q "$wm"; then
                window_manager="$wm"
            fi
        done
    fi
    echo -n "$window_manager"
}

#################################################################
# ┏━╸┏━╸╺┳╸   ╺┳╸┏━╸┏━┓┏┳┓╻┏┓╻┏━┓╻  
# ┃╺┓┣╸  ┃     ┃ ┣╸ ┣┳┛┃┃┃┃┃┗┫┣━┫┃  
# ┗━┛┗━╸ ╹     ╹ ┗━╸╹┗╸╹ ╹╹╹ ╹╹ ╹┗━╸
#################################################################
#################################################################
getterm()
{
    terminal="idfk"
    case "$TERM" in
        "rxvt-unicode" | "rxvt-unicode-256color" )
            terminal="urxvt"
            ;;
        * )
            if command -v xdotool $> /dev/null &&\
                xdotool getactivewindow 2> /dev/null $> /dev/null; then
                terminal=$(ps -aux | grep -F $(xdotool getwindowfocus getwindowpid) | grep -v grep | awk '{print $11}')
            else
                terminal="$TERM"
            fi
            ;;
    esac
    echo "$terminal"
}

#################################################################
# ┏━╸┏━╸╺┳╸   ┏━┓┏━┓┏━╸╻┏ ┏━┓┏━╸┏━╸   ┏━╸┏━┓╻ ╻┏┓╻╺┳╸
# ┃╺┓┣╸  ┃    ┣━┛┣━┫┃  ┣┻┓┣━┫┃╺┓┣╸    ┃  ┃ ┃┃ ┃┃┗┫ ┃ 
# ┗━┛┗━╸ ╹    ╹  ╹ ╹┗━╸╹ ╹╹ ╹┗━┛┗━╸   ┗━╸┗━┛┗━┛╹ ╹ ╹
#################################################################
# Added support for the following
#   - pacman
#   - dpkg
#   - pkg_info
#   - xbps-query
#################################################################
getpackages()
{
    packages="infinite"
    for cmd in pacman dpkg pkg_info xbps-query; do
        if command -v "$cmd" $> /dev/null; then
            pacman="$cmd"
            break
        fi
    done

    if [ ! -z "$pacman" ]; then
        case "$pacman" in
            "pacman" )
                packages="$(pacman -Qq | wc -l)"
                ;;
            "dpkg" )
                packages="$(dpkg --list | wc -l)"
                ;;
            "xbps-query" )
                packages="$(xbps-query -l | wc -l)"
                ;;
            "pkg_info -a" )
                packages="$(pkg_info -a | wc -l)"
                ;;
            * )
                ;;
        esac
    fi
    echo -n "$packages"
}

#################################################################
# ┏┳┓┏━┓╻ ╻┏━╸┏┳┓┏━╸┏┓╻╺┳╸   ┏━╸┏━┓┏┳┓┏┳┓┏━┓┏┓╻╺┳┓┏━┓
# ┃┃┃┃ ┃┃┏┛┣╸ ┃┃┃┣╸ ┃┗┫ ┃    ┃  ┃ ┃┃┃┃┃┃┃┣━┫┃┗┫ ┃┃┗━┓
# ╹ ╹┗━┛┗┛ ┗━╸╹ ╹┗━╸╹ ╹ ╹    ┗━╸┗━┛╹ ╹╹ ╹╹ ╹╹ ╹╺┻┛┗━┛
#################################################################
#################################################################
# followig commands
#       - up                -- move cursor up
#       - down              -- move cursor down
#       - left              -- move cursor left
#       - right             -- move cursor right
#       - move              -- move to (line, column)
#       - resetcursor       -- move cursor to (0, 0)
#       - savecursor        -- save cursor position
#       - restorecursor     -- move cursor back
#################################################################
_u="\033[1A"
_d="\033[1B"
_l="\033[1D"
_r="\033[1C"

up(){ echo -en "\033[$1A"; }
down(){ echo -en "\033[$1B";}
left(){ echo -en "\033[$1D"; }
right(){ echo -en "\033[$1C"; }
move(){ echo -en "\033[$1;$2H"; }
resetcursor(){ echo -en "\033[2J"; }
savecursor(){ echo -en "\033[s"; }
restorecursor(){ echo -en "\033[u"; }

#################################################################
# ╺┳┓┏━╸┏┓ ╻ ╻┏━╸
#  ┃┃┣╸ ┣┻┓┃ ┃┃╺┓
# ╺┻┛┗━╸┗━┛┗━┛┗━┛
#################################################################
#################################################################
dblog "------------------------"
dblog "DETECTED SYSTEM SETTINGS"
dblog "------------------------"
dblog "$(getos)"
dblog "$(getram used) of $(getram total)"
dblog "$(getshell)"
dblog "$(getwm)"
dblog "$(getterm)"
dblog "$(getpackages)"

if [ "$DEBUG" = "true" ]; then
    dblog "exiting before animation"
    exit 0
fi

#################################################################
# ┏━╸┏━╸╺┳╸┏━╸╻ ╻   ┏━┓┏┓╻╻┏┳┓┏━┓╺┳╸╻┏━┓┏┓╻
# ┣╸ ┣╸  ┃ ┃  ┣━┫   ┣━┫┃┗┫┃┃┃┃┣━┫ ┃ ┃┃ ┃┃┗┫
# ╹  ┗━╸ ╹ ┗━╸╹ ╹   ╹ ╹╹ ╹╹╹ ╹╹ ╹ ╹ ╹┗━┛╹ ╹
#################################################################
#################################################################
esc="\033"
f1="${esc}[31m"; f2="${esc}[32m"; f3="${esc}[33m"; f4="${esc}[34m";
f5="${esc}[35m"; f6="${esc}[36m"; f7="${esc}[37m";

b1="${esc}[91m"; b2="${esc}[92m"; b3="${esc}[93m"; b4="${esc}[94m";
b5="${esc}[95m"; b6="${esc}[96m"; b7="${esc}[97m";

f=("${f1}" "${f2}" "${f3}" "${f4}" "${f5}" "${f6}" "${f7}")
b=("${b1}" "${b2}" "${b3}" "${b4}" "${b5}" "${b6}" "${b7}")

scrollprint()
{
    offset=0
    length="${#1}"
    bol=1
    for (( offest=0 ; offset < length ; offset++ )); do
        char="${1:offset:1}"
        printf '%s' "$char"
        sleep 0.01
    done
}

# how I want it to work:
# output XX what to output
out()
{
    left 150; right "$(($default_width + 2))"
    val="$2"
    for func in "${default_funcs[@]}" "${extra_functions[@]}"; do
        if [ "$2" = "$func" ]; then
            val="$($2 $3)"
        fi
    done
    echo -ne "${b7}$1 -- $val"
    down 1
}

tput civis

prev_c=0
prev_l=0
step=0
while true; do
    if [ ! "$prev_c" = "$(tput cols)" ] ||\
       [ ! "$prev_l" = "$(tput lines)" ]; then
        clear
        move 0 0
        output
        #build
        prev_c="$(tput cols)"; prev_l="$(tput lines)"
        continue
    fi
    up 50
    left 50
    chars=""
    tempstep="$step"
    for (( line=0 ; line<7 ; line++ )); do
        charline=""
        for (( cols=0 ; cols<7 ; cols++ )); do
            if [ "$cols" = "$tempstep" ] || \
                [ "$cols" = "$((tempstep - 1))" ] || \
                [ "$cols" = "$((tempstep - 2))" ]; then
                charline="$charline  "
            else
                col="${f[$cols]}"
                
                if [ "$cols" = "$((tempstep - 11))" ] || \
                    [ "$cols" = "$((tempstep + 1))" ] || \
                    [ "$cols" = "$((tempstep - 10))" ] || \
                    [ "$cols" = "$((tempstep + 2))" ] || \
                    [ "$cols" = "$((tempstep - 9))" ] || \
                    [ "$cols" = "$((tempstep + 3))" ] || \
                    [ "$cols" = "$((tempstep - 8))" ] || \
                    [ "$cols" = "$((tempstep + 4))" ];then
                    col="${b[$cols]}"
                fi
                charline="$charline${col}█ "
            fi
        done

        chars="$chars$charline\n"
        if [ ! "$tempstep" -gt 10 ]; then
            tempstep=$((tempstep + 1))
        else
            tempstep=0
        fi
    done

    echo -ne "$chars"

    if [ "$step" -lt "1" ]; then
        step=11
    else
        step=$((step - 1))
    fi

    read -t "$speed" -N 1 input
    if [ ! "$input" = "" ]; then
        exit 0
    fi
    #sleep 0.04
done

