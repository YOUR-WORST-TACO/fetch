#!/bin/sh
# ╺┳╸┏━┓┏━╸┏━┓╻┏━┓   ┏━╸┏━╸╺┳╸┏━╸╻ ╻
#  ┃ ┣━┫┃  ┃ ┃ ┗━┓   ┣╸ ┣╸  ┃ ┃  ┣━┫
#  ╹ ╹ ╹┗━╸┗━┛ ┗━┛   ╹  ┗━╸ ╹ ┗━╸╹ ╹
#       its crap but yer usin it :D
# 
# fetch - https://gitlab.com/YOUR-WORST-TACO/fetch.git
#
# Copyright (C) 2018 Stephen Tafoya <stephentafoya@outlook.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

install_dir="/usr/local/bin"

install()
{
    if ! touch -c "$1" 2> /dev/null; then
        tmpsudo="sudo"    
    fi
    cp ./fetch.sh fetch
    chmod +x fetch
    $tmpsudo mv ./fetch "$1"
    echo "$1" > uninstall
    echo "Fetch installed to $1"
    exit 0
}

uninstall()
{
    if [ -f uninstall ]; then
        undir="$(cat ./uninstall)"
        if ! touch -c "$undir" 2> /dev/null; then
            tmpsudo="sudo"
        fi
        $tmpsudo rm "$undir"/fetch
        rm ./uninstall
        echo "Fetch uninstalled"
        exit 0
    else
        echo "ERROR: can't uninstall"
        exit 1
    fi
}

usage()
{
    echo "./install <install dir> (default is /usr/local/bin)"
    exit 0
}

if [ -z "$1" ]; then
    install "$install_dir"
fi

case "$1" in
    "uninstall" )
        uninstall
        ;;
    "help" )
        usage
        ;;
    * )
        install $1
        ;;
esac
