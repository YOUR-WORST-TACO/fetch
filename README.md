# fetch

Just a fetch, dont worry it wont hurt you

```
fetch - Find Every Tiny Crap tHing
	a fetch script I made that has a fun little animation
	Usage: fetch [OPTIONS]... 

	-h     --help                  display this message
	-d     --debug                 debug
    -x                             command readout (best used with debug)
	-G                             display ram in GB
	-s     [speed](default 0.04)   change animation speed
    -a     [animation]             give an animation file(not implemented)
```

## Install
```
./install
```
or
```
./install $HOME/bin
```

## Uninstall
```
./install uninstall
```

## some sexy screenshots
![shot 1](screenshots/shot1.jpg)
![shot 2](screenshots/shot2.jpg)
![shot 3](screenshots/shot3.jpg)

## support
It doesn't have much support, but PR's are more than welcome
